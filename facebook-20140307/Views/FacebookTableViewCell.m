//
//  FacebookTableViewCell.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/6.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "FacebookTableViewCell.h"

@implementation FacebookTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
