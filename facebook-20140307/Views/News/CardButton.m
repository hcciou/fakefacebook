//
//  CardButton.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/10.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "CardButton.h"

@implementation CardButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void) setLayout
{
    self.backgroundColor = [UIColor colorWithHexString:kColorLightGray];
    [self setTitleColor:[UIColor colorWithHexString:kColorDarkGray] forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
