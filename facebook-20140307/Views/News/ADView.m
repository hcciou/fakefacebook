//
//  ADView.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/10.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "ADView.h"

@interface ADView()
@property (nonatomic, strong) UIImageView* adImage;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UILabel* subTitleLabel;
@end

@implementation ADView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void)setLayout
{
    self.backgroundColor = [UIColor colorWithHexString:kColorLightGray];
    [self addSubview: self.adImage];
    [self addSubview: self.titleLabel];
    [self addSubview: self.subTitleLabel];
}

-(UIImageView *)adImage
{
    if (!_adImage) {
        UIImage* image = [UIImage imageNamed:@"avator"];
        _adImage = [[UIImageView alloc] initWithImage:image];
        _adImage.frame = CGRectMake(10, 10, 34, 34);
    }
    return _adImage;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        CGRect frame = CGRectMake(64, 8, 210, 17);
        _titleLabel = [[UILabel alloc] initWithFrame:frame];
        _titleLabel.text = @"年輕的程式人，如果您對這個領域有無比的熱誠，那就出發吧！";
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        [_titleLabel sizeToFit];
    }
    return _titleLabel;
}

- (UILabel *)subTitleLabel
{
    if (!_subTitleLabel) {
        CGRect frame = CGRectMake(64, 44, 280, 17);
        _subTitleLabel = [[UILabel alloc] initWithFrame:frame];
        _subTitleLabel.text = @"www.hcc.taipei";
        _subTitleLabel.font = [UIFont systemFontOfSize:12];
        _subTitleLabel.textColor = [UIColor colorWithHexString:kColorDarkGray];
    }
    return _subTitleLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
