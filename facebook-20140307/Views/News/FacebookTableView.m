//
//  FacebookTableView.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/6.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "FacebookTableView.h"

@implementation FacebookTableView

- (id)initWithFrame:(CGRect)frame
{
//    NSLog(@"aaa");
    self = [super initWithFrame:frame];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void)setLayout
{
    self.backgroundColor = [UIColor colorWithHexString:kColorDarkGray];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
