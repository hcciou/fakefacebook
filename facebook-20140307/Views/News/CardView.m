//
//  CardView.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/10.
//  Copyright (c) 2014年 hcc. All rights reserved.
//
#import "CardView.h"
#import "CardButton.h"
#import "ADView.h"

static const CGFloat buttonHight = 191;

@interface CardView()

@property (nonatomic, strong) UIImageView* avatorImage;
@property (nonatomic, strong) UILabel* nameLabel;
@property (nonatomic, strong) UILabel* dateLabel;
@property (nonatomic, strong) UILabel* likeLabel;
@property (nonatomic, strong) UILabel* contentLabel;
@property (nonatomic, strong) CardButton* likeButton;
@property (nonatomic, strong) CardButton* commetButton;
@property (nonatomic, strong) CardButton* shareButton;
@property (nonatomic, strong) ADView* adView;

@end

@implementation CardView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setLayout];
    }
    return self;
}

- (void) setLayout
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview: self.avatorImage];
    [self addSubview: self.nameLabel];
    [self addSubview: self.dateLabel];
    [self addSubview: self.contentLabel];
    [self addSubview: self.likeLabel];
    [self addSubview: self.adView];
    [self addSubview: self.likeButton];
    [self addSubview: self.commetButton];
    [self addSubview: self.shareButton];
}

-(UIImageView *)avatorImage
{
    if (!_avatorImage) {
        UIImage* image = [UIImage imageNamed:@"avator"];
        _avatorImage = [[UIImageView alloc] initWithImage:image];
        _avatorImage.frame = CGRectMake(10, 10, 34, 34);
    }
    return _avatorImage;
}

- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        CGRect frame = CGRectMake(52, 10, 220, 17);
        _nameLabel = [[UILabel alloc] initWithFrame:frame];
        _nameLabel.textColor = [UIColor colorWithHexString: kColorBlack];
        _dateLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.text = @"hcc";
    }
    return _nameLabel;
}

-(UILabel *)dateLabel
{
    if (!_dateLabel) {
        CGRect frame = CGRectMake(52, 27, 220, 17);
        _dateLabel = [[UILabel alloc] initWithFrame:frame];
        _dateLabel.textColor = [UIColor colorWithHexString: kColorDarkGray];
        _dateLabel.font = [UIFont systemFontOfSize:14];
        _dateLabel.text = @"昨天 下午5:34 在 臺北 附近";
    }
    return _dateLabel;
}

-(UILabel *)contentLabel
{
    if (!_contentLabel) {
        CGRect frame = CGRectMake(10, 58, 280, 17);
        _contentLabel = [[UILabel alloc] initWithFrame:frame];
        _contentLabel.text = @"我還記得我們敬愛的李家同杯杯....";
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _contentLabel.numberOfLines = 0;
        [_contentLabel sizeToFit];
        _contentLabel.font = [UIFont systemFontOfSize:15];
        _contentLabel.textColor = [UIColor colorWithHexString:kColorBlack];
    }
    return _contentLabel;
}

-(ADView *)adView
{
    if (!_adView) {
        CGRect frame = CGRectMake(10, 88, 280, 64);
        _adView = [[ADView alloc] initWithFrame:frame];
    }
    return _adView;
}

- (UILabel *)likeLabel
{
    if (!_likeLabel) {
        CGRect frame = CGRectMake(10, 164, 280, 17);
        _likeLabel = [[UILabel alloc] initWithFrame: frame];
        _likeLabel.text = @"345 個讚";
        _likeLabel.font = [UIFont systemFontOfSize:14];
        _likeLabel.textColor = [UIColor colorWithHexString:kColorDarkGray];
    }
    return _likeLabel;
}

- (CardButton *)likeButton
{
    if (!_likeButton) {
        CGRect frame = CGRectMake(0, buttonHight, 100, 39);
        _likeButton = [[CardButton alloc] initWithFrame:frame];
        [_likeButton setTitle:@"讚" forState:UIControlStateNormal];
    }
    return _likeButton;
}

- (CardButton *)commetButton
{
    if (!_commetButton) {
        CGRect frame = CGRectMake(100, buttonHight, 100, 39);
        _commetButton = [[CardButton alloc] initWithFrame:frame];
        [_commetButton setTitle:@"留言" forState:UIControlStateNormal];
    }
    return _commetButton;
}

- (CardButton *)shareButton
{
    if (!_shareButton) {
        CGRect frame = CGRectMake(200, buttonHight, 100, 39);
        _shareButton = [[CardButton alloc] initWithFrame:frame];
        [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
    }
    return _shareButton;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
