//
//  FacebookTableViewCell.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/6.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "FacebookTableViewCell.h"
#import "CardView.h"

@interface FacebookTableViewCell()
@property (nonatomic, strong) CardView* cardView;
@end

@implementation FacebookTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void)setLayout
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor colorWithHexString:kColorDarkGray];
    [self addSubview: self.cardView];
}

- (CardView *)cardView
{
    if (!_cardView) {
        CGRect frame = CGRectMake(10, 15, 300, 230);
        _cardView = [[CardView alloc] initWithFrame: frame];
//        _cardView.backgroundColor = [UIColor redColor];
    }
    return _cardView;
}

//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end
