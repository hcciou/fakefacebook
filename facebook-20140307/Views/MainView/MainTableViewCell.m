//
//  MainTableViewCell.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/11.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "MainTableViewCell.h"
#import "MainHeadLineView.h"

@interface MainTableViewCell()
@property (nonatomic) MainHeadLineView* headLineView;
@end


@implementation MainTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setLayout
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setMenutype:(MenuType)menutype
{
    switch (menutype)
    {
        case MenuTypeHeadLine:
            [self addSubview: self.headLineView];
            break;
        case MenuTypeNormal:
            break;
    }
}

- (MainHeadLineView *)headLineView
{
    if (!_headLineView) {
        CGFloat width = 8;
        CGRect frame = CGRectMake(width,
                                  width,
                                  self.bounds.size.width - 2*width,
                                  100-2*width);
        _headLineView = [[MainHeadLineView alloc] initWithFrame:frame];
    }
    return _headLineView;
}

@end
