//
//  MainTableViewCell.h
//  facebook-20140307
//
//  Created by hcc on 2014/3/11.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum MenuType
{
    MenuTypeHeadLine,
    MenuTypeNormal,
} MenuType;

@interface MainTableViewCell : UITableViewCell
@property (nonatomic) MenuType menutype;
@end
