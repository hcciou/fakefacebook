//
//  MenuTableView.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/11.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "MainTableView.h"

@implementation MainTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void)setLayout
{
//    self.backgroundColor = [UIColor redColor];
//    線劃到底
    self.separatorInset = UIEdgeInsetsZero;


}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
