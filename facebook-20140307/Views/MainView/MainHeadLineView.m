//
//  MainHeadLineView.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/11.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "MainHeadLineView.h"

@interface MainHeadLineView()
@property (nonatomic, strong) UIImageView* avatorImageView;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UILabel* subtitleLabel;
@end

@implementation MainHeadLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void) setLayout
{
    [self addSubview: self.avatorImageView];
    [self addSubview: self.titleLabel];
    [self addSubview: self.subtitleLabel];
}

- (UIImageView *)avatorImageView
{
    if (!_avatorImageView) {
        _avatorImageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"avator"]];
        _avatorImageView.frame = CGRectMake(0, 0, 84, 84);
    }
    return _avatorImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        CGRect frame = CGRectMake(100, 0, 154, 30);
        _titleLabel = [[UILabel alloc] initWithFrame:frame];
        _titleLabel.text = @"GGGGGG";
        _titleLabel.font = [UIFont systemFontOfSize:15];
    }
    return _titleLabel;
}

-(UILabel *)subtitleLabel
{
    if (!_subtitleLabel) {
        CGRect frame = CGRectMake(100, 38, 154, 30);
        _subtitleLabel = [[UILabel alloc] initWithFrame:frame];
        _subtitleLabel.text = @"123456789";
        _subtitleLabel.font = [UIFont systemFontOfSize:10];
        _subtitleLabel.textColor = [UIColor colorWithHexString:kColorDarkGray];
    }
    return _subtitleLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
