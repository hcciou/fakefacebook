//
//  ColorList.h
//  facebook-20140307
//
//  Created by hcc on 2014/3/6.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

static NSString* const kColorBlue = @"4866a4";
static NSString* const kColorBlack = @"141823";
static NSString* const kColorDarkGray = @"d3d6db";
static NSString* const kColorLightGray = @"f7f7f7";