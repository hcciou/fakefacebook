//
//  MainViewController.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/11.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "MainViewController.h"
#import "MainTableView.h"
#import "MainTableViewCell.h"

@interface MainViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) MainTableView* tableView;
@property (nonatomic, strong) MainTableViewCell* tableViewCell;
@property (nonatomic, strong) NSArray* itemArray;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview: self.tableView];
    [self setNavigationBar];
//    [self.view addSubview: self.tableViewCell];
}

- (void)setNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:kColorBlue];

}

- (MainTableView *)tableView
{
    if (!_tableView) {
        CGRect frame = self.view.bounds;
//        frame.size.width = 270;
//        frame.origin.y = 64;
        _tableView = [[MainTableView alloc] initWithFrame:frame];
//        _tableView.backgroundColor = [UIColor redColor];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    return _tableView;
}

-(NSArray *)itemArray
{
    if (!_itemArray) {
        _itemArray = @[@"你可能認識",
                       @"職缺",
                       @"人脈",
                       @"公司",
                       @"社團",
                       @"家人"];
    }
    return _itemArray;
}

- (NSInteger)tableView:(MainTableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
};
- (MainTableViewCell *)tableView:(MainTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"mainCellID"];
    if (!cell) {
        cell = [[MainTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mainCellID"];
    }
    
    if ( indexPath.row < 2 ) {
        cell.menutype = MenuTypeHeadLine;
    }
    else
    {
        UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"Item%ld", indexPath.row-1]];
        cell.menutype = MenuTypeNormal;
        cell.textLabel.text = self.itemArray[indexPath.row -2];;
        cell.imageView.image = image;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 2) {
        return 100;
    }
    else
    {
        return 40;
    }
}
@end
