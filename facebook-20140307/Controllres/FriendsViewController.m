//
//  FriendsViewController.m
//  FacebookUI
//
//  Created by hcc on 2014/3/11.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendsTableView.h"
#import "FriendsTableViewCell.h"
@interface FriendsViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) FriendsTableView* tableView;
@property (strong, nonatomic) FriendsTableViewCell* tableViewCell;
@end

@implementation FriendsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview: self.tableView];
    [self.view addSubview: self.tableViewCell];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (FriendsTableView *)tableView
{
    if (!_tableView) {
        CGRect frame = self.view.bounds;
        _tableView = [[FriendsTableView alloc] initWithFrame:frame];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

- (NSInteger)tableView:(FriendsTableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (FriendsTableViewCell *)tableView:(FriendsTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendsTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"friendsCellID"];
    if (!cell) {
        cell = [[FriendsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"friendsCellID"];
    }
    cell.textLabel.text = @"FFF";
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
;