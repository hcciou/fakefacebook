//
//  ViewController.m
//  facebook-20140307
//
//  Created by hcc on 2014/3/6.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "NewsViewController.h"
#import "FacebookTableView.h"
#import "FacebookTableViewCell.h"

@interface NewsViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) FacebookTableView* tableView;
@end

@implementation NewsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBar];
    [self.view addSubview: self.tableView];
}

- (void)setNavigationBar
{
    self.title =@"動態時報";
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"4866a4"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
}

-(FacebookTableView *)tableView
{
    if (!_tableView) {
        CGRect frame = self.view.frame;
//        frame.origin.y = 64;
        _tableView = [[FacebookTableView alloc] initWithFrame:frame ];
//        _tableView.backgroundColor = [UIColor colorWithHexString:kColorDarkGray];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

#pragma mark - Datasource Delegate
- (NSInteger)tableView:(FacebookTableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (FacebookTableViewCell *)tableView:(FacebookTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FacebookTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"fbCellID"];
    if (!cell) {
        cell = [[FacebookTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"fbCellID"];
    }
//    cell.textLabel.text = @"wwwwwww";
    return cell;
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 245;
}


@end
