//
//  AppDelegate.h
//  facebook-20140307
//
//  Created by hcc on 2014/3/6.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JASidePanelController.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JASidePanelController* viewController;
@end
